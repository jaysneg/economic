class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :title
      t.string :description
      t.decimal :price, precision: 8, scale: 2
      t.belongs_to :category, index: true
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
