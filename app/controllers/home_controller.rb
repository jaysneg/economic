require 'date'

class HomeController < ApplicationController
  def index
    date = Time.now

    @categories = current_user.categories
    @from = date.beginning_of_year
    @to = date.end_of_year

    if params[:scope].present?
      case params[:scope]
      when 'day'
        @from = date.beginning_of_day
        @to = date.end_of_day
      when 'month'
        @from = date.beginning_of_month
        @to = date.end_of_month
      when 'year'
        @from = date.beginning_of_year
        @to = date.end_of_year
      end
    end

    if params[:from].present?
      begin
        @from = DateTime.parse(params[:from])
      rescue
      end
    end

    if params[:to].present?
      begin
        @to = DateTime.parse(params[:to])
      rescue
      end

    end

    sum_lambda = ->(k, v) { { label: k, y: v.sum {|e| e.price.to_f}} }

    expenses = current_user.expenses.includes(:category).where(:purchase_date => @from..@to)
    expenses = expenses.where(category: params[:categories]) if params[:categories].present?

    @by_month = expenses.group_by {|i| i.purchase_date.strftime("%B") }.map(&sum_lambda)
    @by_categories = expenses.group_by {|i| i.category.title }.map(&sum_lambda)
    @by_title = expenses.group_by { |i| i.title }.map(&sum_lambda)
  end
end
