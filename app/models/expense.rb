class Expense < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  before_save :title_capitalize

  validates :title, :price, :category_id, presence: true

  paginates_per 100

  def title_capitalize
    self.title = Unicode::capitalize(title.strip)
  end

end
