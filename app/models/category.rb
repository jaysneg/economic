class Category < ActiveRecord::Base
  has_many :expences
  belongs_to :user
  before_save :title_capitalize

  validates :title, presence: true
  validates_uniqueness_of :title, scope: :user_id

  paginates_per 100

  def title_capitalize
    self.title = Unicode::capitalize(title.strip)
  end


end
