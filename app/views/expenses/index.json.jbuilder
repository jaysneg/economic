json.array!(@expenses) do |expense|
  json.extract! expense, :id, :title, :description, :price
  json.url expense_url(expense, format: :json)
end
