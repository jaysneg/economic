module HomeHelper
  def chart_dates(from, to)
    res = ''
    res << " с #{from} " if from.present?
    res << " до #{to} " if to.present?

    res
  end

  def check_category_status(params, item_id)
    res = false

    if params['categories'].present?
      res = params['categories'].include?(item_id.to_s)
    end

    res
  end
end
